﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Otus.Teaching.Pcf.Administration.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;

namespace Otus.Teaching.Pcf.Administration.Core.Services
{

    //=====================================================================================================
    public interface IAdmBLL
    {
        Task<bool> UpdatePromocodeCntAsync(Guid id);
    }


    public struct mqMsg
    {
        public Guid id { get; set; }
        public string msg { get; set; }
    }

    //=====================================================================================================
    public  class AdmBLL : IAdmBLL
    {
        private readonly IRepository<Employee> _repo;
        //------------------------------------------------------------------------------------------
        public AdmBLL(IRepository<Employee> repo)
        {
            _repo = repo;
        }
        
        //------------------------------------------------------------------------------------------
        public async Task<bool> UpdatePromocodeCntAsync(Guid id)
        {
            var employee = await _repo.GetByIdAsync(id);

            if (employee == null)
                return false;

            employee.AppliedPromocodesCount++;

            await _repo.UpdateAsync(employee);
            return true;
        }
    }
}
