﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;

namespace Otus.Teaching.Pcf.GivingToCustomer.Core.Services
{
    //=====================================================================================================
    public interface IGTCBLL
    {
        Task<bool> GivePromocodeAsync(GivePromoCodeToCustomerDto dto);
    }


   

    //=====================================================================================================
    public class GTCBLL : IGTCBLL
    {
        private readonly IRepository<Preference> _repoPref;
        private readonly IRepository<Customer> _repoCust;
        private readonly IRepository<PromoCode> _repoPromo;
        //------------------------------------------------------------------------------------------
        public GTCBLL(IRepository<Preference> repoPref, IRepository<Customer> repoCust, IRepository<PromoCode> repoPromo)
        {
            _repoPref = repoPref;
            _repoCust = repoCust;
            _repoPromo = repoPromo;
        }

        //------------------------------------------------------------------------------------------
        public async Task<bool> GivePromocodeAsync(GivePromoCodeToCustomerDto dto)
        {
            //Получаем предпочтение по имени
            var preference = await _repoPref.GetByIdAsync(dto.PreferenceId);

            if (preference == null)
            {
                return false;
            }

            //  Получаем клиентов с этим предпочтением:
            var customers = await _repoCust
                .GetWhere(d => d.Preferences.Any(x =>
                    x.Preference.Id == preference.Id));

            PromoCode promoCode = PromoCodeMapper.MapFromModel(dto, preference, customers);

            await _repoPromo.AddAsync(promoCode);
            return true;
        }
    }

    public class GivePromoCodeToCustomerDto
    {
        public string ServiceInfo { get; set; }

        public Guid PartnerId { get; set; }

        public Guid PromoCodeId { get; set; }

        public string PromoCode { get; set; }

        public Guid PreferenceId { get; set; }

        public string BeginDate { get; set; }

        public string EndDate { get; set; }

        public Guid? PartnerManagerId { get; set; }
    }
    public class PromoCodeMapper
    {
        public static PromoCode MapFromModel(GivePromoCodeToCustomerDto dto, Preference preference, IEnumerable<Customer> customers)
        {

            var promocode = new PromoCode();
            promocode.Id = dto.PromoCodeId;

            promocode.PartnerId = dto.PartnerId;
            promocode.Code = dto.PromoCode;
            promocode.ServiceInfo = dto.ServiceInfo;

            promocode.BeginDate = DateTime.Parse(dto.BeginDate);
            promocode.EndDate = DateTime.Parse(dto.EndDate);

            promocode.Preference = preference;
            promocode.PreferenceId = preference.Id;

            promocode.Customers = new List<PromoCodeCustomer>();

            foreach (var item in customers)
            {
                promocode.Customers.Add(new PromoCodeCustomer()
                {

                    CustomerId = item.Id,
                    Customer = item,
                    PromoCodeId = promocode.Id,
                    PromoCode = promocode
                });
            };

            return promocode;
        }
    }
}
