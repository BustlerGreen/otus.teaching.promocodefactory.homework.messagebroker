﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Services;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Text;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;

namespace Otus.Teaching.PcfGivingToCustomer.WebHost.HostedService
{
    public class EventListenerMQ : BackgroundService
    {
        private readonly IConnection _connection;
        private readonly IModel _channel;
        private readonly string _queueName;

        public IServiceProvider Services { get; }
        public EventListenerMQ(IServiceProvider services)
        {
            Services = services;

            var factory = new ConnectionFactory(){ HostName = "localhost", UserName = "promomq", Password = "promomq" };
            _connection = factory.CreateConnection();
            _channel = _connection.CreateModel();
            _channel.ExchangeDeclare(exchange: "promo_mq", type: "topic");
            _queueName = _channel.QueueDeclare().QueueName;
            _channel.QueueBind(queue: _queueName,
                exchange: "promo_mq",
                routingKey: "GivePromo"
                );

        }
        
        protected override Task ExecuteAsync(CancellationToken ct)
        {
            ct.ThrowIfCancellationRequested();

            var consumer = new EventingBasicConsumer(_channel);
            consumer.Received += (model, ea) =>
            {
                var body = JsonSerializer.Deserialize<GivePromoCodeToCustomerDto>(Encoding.UTF8.GetString(ea.Body.ToArray()));
                using (var scope = Services.CreateScope())
                {
                    var scopedService = scope.ServiceProvider.GetRequiredService<IGTCBLL>();
                    Task<bool> task = Task.Run<bool>(async () => await scopedService.GivePromocodeAsync(body));
                    var res = task.Result;
                }
                _channel.BasicAck(ea.DeliveryTag, false);
            };
            _channel.BasicConsume(queue: _queueName,
                autoAck: false,
                consumer: consumer
                );
            return Task.CompletedTask;

        }

      

        public override async Task StopAsync(CancellationToken ct)
        {
            await base.StopAsync(ct);
        }

        public override void Dispose()
        {
            base.Dispose();
            _channel?.Dispose();
            _connection?.Dispose();
        }
    }
}
