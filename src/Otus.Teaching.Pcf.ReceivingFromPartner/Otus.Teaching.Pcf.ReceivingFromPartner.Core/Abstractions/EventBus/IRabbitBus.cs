﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.Core.Abstractions.EventBus
{
    public interface IRabbitBus
    {
        public void RiseEvent(Object obj, Type ctype,  string key);
        public void RiseEvent(string message, string key);
    }
}
