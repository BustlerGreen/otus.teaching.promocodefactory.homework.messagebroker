﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json;
using RabbitMQ.Client;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.Integration
{
    public class RabbitMQProducer : Core.Abstractions.EventBus.IRabbitBus, IDisposable
    {
        protected readonly ConnectionFactory _factory = null;
        protected readonly IModel _channel = null;
        private readonly IConnection _connection = null;

        //--------------------------------------------------------------------------------------------------
        public RabbitMQProducer()
        {
            _factory = new ConnectionFactory() { HostName = "localhost", UserName = "promomq", Password = "promomq" };
            _connection = _factory.CreateConnection();
            _channel = _connection.CreateModel();
            _channel.ExchangeDeclare(exchange: "promo_mq", type: "topic");

            
        }

        //--------------------------------------------------------------------------------------------------
        public void RiseEvent(Object obj, Type ctype, string key)
        {
            string so = JsonSerializer.Serialize(obj, ctype);
            RiseEvent(so, key);

        }

        //--------------------------------------------------------------------------------------------------
        public void RiseEvent(string message, string key)
        {
            var body = Encoding.UTF8.GetBytes(message);
            _channel.BasicPublish(exchange: "promo_mq",
                routingKey: key,
                basicProperties: null,
                body: body 
                );
        }

        //--------------------------------------------------------------------------------------------------
        public void Dispose()
        {
            _channel?.Dispose();
            _connection?.Dispose();
        }
    }
}
